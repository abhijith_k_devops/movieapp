package com.cs.movieapp.data.model

data class Response(
    val page: Page
) {
    data class Page(
        val content_items: ContentItems,
        val page_num: String,
        val page_size: String,
        val title: String,
        val total_content_items: String
    ) {
        data class ContentItems(
            val content: List<Content>
        ) {
            data class Content(
                val name: String,
                val poster_image: String
            )
        }
    }
}