package com.cs.movieapp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.movieapp.data.model.Movies

@Dao
interface MovieDao:BaseDao<Movies> {

    @Query("SELECT * FROM ${Movies.TABLE_NAME}")
    fun selectMovies(): List<Movies>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movies: Movies) : Long



}