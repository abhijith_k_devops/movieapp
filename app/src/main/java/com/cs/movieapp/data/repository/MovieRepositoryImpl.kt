package com.cs.movieapp.data.repository

import android.content.Context
import android.util.Log
import com.cs.movieapp.data.local.dao.MovieDao
import com.cs.movieapp.data.model.Movies
import com.cs.movieapp.utils.PreferenceHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class MovieRepositoryImpl(val movieDao: MovieDao,val preferenceHelper: PreferenceHelper):MovieRepository {
    override suspend fun fetchMovies(): Flow<List<Movies>>  = flow {
        val result = movieDao.selectMovies()
        emit(result)
    }.flowOn(Dispatchers.IO)

    override suspend fun createMovie(movies: Movies) {
        movieDao.insert(movies)
    }

}