package com.cs.movieapp.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.cs.movieapp.data.local.dao.MovieDao
import com.cs.movieapp.data.model.Movies

@Database(
    entities = [Movies::class],
    version = DatabaseMigrations.DB_VERSION
)
abstract class MovieDb : RoomDatabase() {
    abstract fun movieDao(): MovieDao

    companion object {
        private const val DB_NAME = "movie_app"

        @Volatile
        private var INSTANCE: MovieDb? = null

        fun getInstance(context: Context): MovieDb =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, MovieDb::class.java, DB_NAME).build()
    }
}