package com.cs.movieapp.data.repository

import android.content.Context
import com.cs.movieapp.data.model.Movies
import kotlinx.coroutines.flow.Flow

interface MovieRepository {

    suspend fun fetchMovies(): Flow<List<Movies>>

    suspend fun createMovie(movies: Movies)

}