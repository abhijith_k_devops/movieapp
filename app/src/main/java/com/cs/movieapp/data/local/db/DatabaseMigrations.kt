package com.cs.movieapp.data.local.db

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

object DatabaseMigrations {
    const val DB_VERSION = 1

    val MIGRATIONS: Array<Migration>
        get() = arrayOf<Migration>(
            //migration1()
        )

   /* private fun migration1(): Migration = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE ${User.TABLE_NAME} ADD COLUMN all_files_uploaded INTEGER DEFAULT 0 NOT NULL")
        }
    }*/
//    private fun migration1(): Migration = object : Migration(1, 2) {
//        override fun migrate(database: SupportSQLiteDatabase) {
//            database.execSQL("ALTER TABLE ${User.TABLE_NAME} ALTER COLUMN id TEXT")
//        }
//    }
}