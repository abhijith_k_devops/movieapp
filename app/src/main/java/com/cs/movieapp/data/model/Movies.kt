package com.cs.movieapp.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.cs.movieapp.data.model.Movies.Companion.TABLE_NAME


@Entity(tableName = TABLE_NAME)
data class Movies(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val title: String,
    val picture: String
){
    companion object {
        const val TABLE_NAME = "task_movies"
    }
}
