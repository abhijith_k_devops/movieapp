package com.cs.movieapp.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.cs.movieapp.R
import com.cs.movieapp.data.model.Movies
import com.cs.movieapp.data.model.Response
import com.cs.movieapp.databinding.ActivitySplashBinding
import com.cs.movieapp.ui.main.MainActivity
import com.cs.movieapp.utils.AppConstants
import com.cs.movieapp.utils.PreferenceHelper
import com.squareup.moshi.JsonAdapter
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset
import org.json.JSONObject
import retrofit2.converter.moshi.MoshiConverterFactory
import com.squareup.moshi.Moshi





class SplashActivity : AppCompatActivity() {

    val viewModel: SplashViewModel by viewModel()
    val preferenceHelper: PreferenceHelper by inject()
    lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (preferenceHelper.getBooleanValue(AppConstants.IS_NOT_FIRST_TIME)){
            startActivity(Intent(this,MainActivity::class.java))
        }else {
            val obj = JSONObject(loadJSONFromAsset("CONTENTLISTINGPAGE-PAGE1.json"))
            val response = toClass(obj.toString())
            for (content in response.page.content_items.content){
                viewModel.createMovie(Movies(title = content.name,picture = content.poster_image))
            }
            startActivity(Intent(this,MainActivity::class.java))
        }


    }
    fun loadJSONFromAsset(file: String): String? {
        var json: String? = null
        json = try {
            val `is`: InputStream = resources.assets.open(file)
            val size: Int = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }
    fun toClass(json: String): Response{
        val moshi = Moshi.Builder().build()
        val adapter: JsonAdapter<Response> = moshi.adapter<Response>(Response::class.java)

        return adapter.fromJson(json)!!
    }
}