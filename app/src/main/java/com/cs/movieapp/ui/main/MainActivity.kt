package com.cs.movieapp.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.cs.movieapp.R
import com.cs.movieapp.data.model.Movies

import com.cs.movieapp.databinding.ActivityMainBinding
import com.cs.movieapp.ui.main.MainViewModel
import org.jetbrains.anko.dimen
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.view.Menu

import android.view.MenuInflater
import com.cs.movieapp.utils.AppConstants
import com.cs.movieapp.utils.PaginationScrollListener
import com.cs.movieapp.utils.PreferenceHelper
import org.koin.android.ext.android.inject

import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import org.jetbrains.anko.toast


class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    val viewModel: MainViewModel by viewModel()
    lateinit var gridLayoutManager: GridLayoutManager
    val preferenceHelper: PreferenceHelper by inject()
    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var list = ArrayList<Movies>()
    lateinit var  adapter: MovieRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setSupportActionBar(binding.tbHome)
        supportActionBar?.setHomeAsUpIndicator(resources.getDrawable(R.drawable.back))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewModel.fetchMovie()
        val column = resources.getInteger(R.integer.gallery_columns)
        gridLayoutManager = GridLayoutManager(this,column)
        binding.rvMovie.layoutManager = gridLayoutManager
        viewModel.selectMovieState.observe(this, Observer {
            list = it as ArrayList<Movies>
            adapter = MovieRecyclerViewAdapter(this,list)
            binding.rvMovie.adapter = adapter
        })

        viewModel.paginatedMovieState.observe(this, Observer {
            adapter.addItem(it as ArrayList<Movies>)
            isLoading = false
            isLastPage = false
        })
        binding.rvMovie.addOnScrollListener(object : PaginationScrollListener(gridLayoutManager){
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems(currentPage: Int) {
                viewModel.selectPaginatedMovie(currentPage)
                isLoading = true
            }

        })
        setContentView(binding.root)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        val search: MenuItem = menu!!.findItem(R.id.search)
        val searchView: SearchView = search.getActionView() as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.length >= 3){
                    filter(query)
                    return true
                }else{
                    toast("Must enter query of length 3 or more")
                    return false
                }


            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText!!.length >= 3){
                    filter(newText)
                    return true
                }else{
                    adapter.updateList(list)
                    toast("Must enter query of length 3 or more")
                    return false
                }
            }

        })
        return true
    }
    fun filter(text: CharSequence) {
        val temp : ArrayList<Movies> = ArrayList()
        if (list.size>0) {
            for (d in list) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.title.toLowerCase().contains(text)) {
                    temp.add(d)
                }

            }
            //update recyclerview
            adapter.updateList(temp)
        }else{
            toast("No result found")
        }
    }
}