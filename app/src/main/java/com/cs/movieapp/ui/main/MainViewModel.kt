package com.cs.movieapp.ui.main

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.cs.movieapp.data.model.Movies
import com.cs.movieapp.data.model.Response
import com.cs.movieapp.data.repository.MovieRepository
import com.cs.movieapp.utils.AppConstants
import com.cs.movieapp.utils.PreferenceHelper
import com.cs.movieapp.utils.validation.LiveDataValidator
import com.cs.movieapp.utils.validation.LiveDataValidatorResolver
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

class MainViewModel(
    val movieRepository: MovieRepository,
    val preferenceHelper: PreferenceHelper,
    val context: Context,
) : ViewModel() {
    private var _selectMovieState : MutableLiveData<List<Movies>> = MutableLiveData()
    val selectMovieState : LiveData<List<Movies>> = _selectMovieState

    private var _paginatedMovieState : MutableLiveData<List<Movies>> = MutableLiveData()
    val paginatedMovieState : LiveData<List<Movies>> = _paginatedMovieState


    fun fetchMovie(){
        viewModelScope.launch {
            preferenceHelper.setBooleanValue(AppConstants.IS_NOT_FIRST_TIME,true)
            movieRepository.fetchMovies()
                .collect { _selectMovieState.value = it }
        }
    }
    fun selectPaginatedMovie(page: Int){
        viewModelScope.launch {
            if (page<=3) {
                var list = ArrayList<Movies>()
                val obj = JSONObject(loadJSONFromAsset("CONTENTLISTINGPAGE-PAGE${page}.json"))
                val response = toClass(obj.toString())
                for (content in response.page.content_items.content) {
                    list.add(Movies(title = content.name, picture = content.poster_image))
                }
                _paginatedMovieState.value = list
            }
        }
    }
    fun loadJSONFromAsset(file: String): String? {
        var json: String? = null
        json = try {
            val `is`: InputStream = context.resources.assets.open(file)
            val size: Int = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }
    fun toClass(json: String): Response {
        val moshi = Moshi.Builder().build()
        val adapter: JsonAdapter<Response> = moshi.adapter<Response>(Response::class.java)

        return adapter.fromJson(json)!!
    }

}