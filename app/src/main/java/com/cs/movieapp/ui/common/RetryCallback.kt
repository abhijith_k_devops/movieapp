package  com.cure.curedeliveryboy.ui.common

interface RetryCallback {
    fun retry()
}