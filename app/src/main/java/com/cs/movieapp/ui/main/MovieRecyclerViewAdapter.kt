package com.cs.movieapp.ui.main

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cs.movieapp.R
import com.cs.movieapp.data.model.Movies
import com.cs.movieapp.databinding.LayoutItemBinding
import org.jetbrains.anko.imageResource
import android.app.Activity

import android.util.DisplayMetrics
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.cs.movieapp.ui.common.DataBoundPagingDataAdapter


class MovieRecyclerViewAdapter(val context: Context,var list: ArrayList<Movies>) : RecyclerView.Adapter<MovieRecyclerViewAdapter.MovieViewHolder>(){
     var isEmpty: Boolean = false
     class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
         val binding = LayoutItemBinding.bind(itemView)
     }

     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
         val itemView = LayoutInflater.from(context).inflate(R.layout.layout_item, parent, false)
         val viewHolder = MovieViewHolder(
             itemView
         )
         return viewHolder
     }

     override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
         val movie = list.get(position)
         holder.binding.tvMovieName.text = movie.title
         val resourceImage: Int = context.getResources()
             .getIdentifier(movie.picture.split(".").get(0), "drawable", context.getPackageName())
         if (resourceImage == 0)
             holder.binding.ivPoster.setImageDrawable(context.resources.getDrawable(R.drawable.placeholder_for_missing_posters))
          else
             holder.binding.ivPoster.setImageDrawable(context.resources.getDrawable(resourceImage))
     }

     fun addItem(listed: ArrayList<Movies>) {
         this.list.addAll(listed)
         notifyDataSetChanged()
     }
     override fun getItemCount(): Int {
         return list.size
     }
     fun updateList(list: ArrayList<Movies>) {
         this.list = list
         isEmpty = list!!.size==0
         notifyDataSetChanged()
     }


}