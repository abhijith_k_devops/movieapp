package com.cs.movieapp.ui.splash

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cs.movieapp.data.model.Movies
import com.cs.movieapp.data.model.Response
import com.cs.movieapp.data.repository.MovieRepository
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.IOException
import java.io.InputStream

class SplashViewModel(val movieRepository: MovieRepository,val context: Context): ViewModel() {
    fun createMovie(movies: Movies){
        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            movieRepository.createMovie(movies)
        }
    }
    fun loadJSONFromAsset(file: String): String? {
        var json: String? = null
        json = try {
            val `is`: InputStream = context.resources.assets.open(file)
            val size: Int = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }
    fun toClass(json: String): Response {
        val moshi = Moshi.Builder().build()
        val adapter: JsonAdapter<Response> = moshi.adapter<Response>(Response::class.java)

        return adapter.fromJson(json)!!
    }
}