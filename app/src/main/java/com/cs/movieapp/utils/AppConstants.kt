package com.cs.movieapp.utils

object AppConstants {

    val PAGE_ADDED: String = "pages_added"
    val IS_NOT_FIRST_TIME: String = "is_not_first_time"

}