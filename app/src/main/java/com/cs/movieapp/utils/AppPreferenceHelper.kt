package com.cs.movieapp.utils

import android.content.Context
import android.content.SharedPreferences

object PrefKeys {

    const val IS_LOGIN = "isLogin"
}

class PreferenceHelperImpl(_context: Context) : PreferenceHelper {

    private val prefName = "com.cure.curecaptain.pref"
    private val privateMode = 0
    private val mPrefs: SharedPreferences
    private val editor: SharedPreferences.Editor

    init {
        mPrefs = _context.getSharedPreferences(prefName, privateMode)
        editor = mPrefs.edit()
    }



    override fun setBooleanValue(key : String, value: Boolean) {
        editor.putBoolean(key, value)
        editor.commit()
    }

    override fun getBooleanValue(key : String): Boolean {
        return mPrefs.getBoolean(key, false)
    }

    override fun setStringValue(key: String, value: String) {
        editor.putString(key, value)
        editor.commit()
    }

    override fun getStringValue(key: String): String? {
        return mPrefs.getString(key, null)
    }

    override fun setIntValue(key: String, value: Int) {
        editor.putInt(key, value)
        editor.commit()
    }

    override fun getIntValue(key: String): Int? {
        return mPrefs.getInt(key, 0)
    }


    override fun clearPreference() {
        editor.clear()
        editor.commit()
    }

}
