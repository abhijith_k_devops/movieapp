package com.cs.movieapp.utils.validation

class LiveDataValidatorResolver(private val validators: List<LiveDataValidator>) {
    fun isValid(): Boolean {
        var valid = true
        for (validator in validators) {
            if (!validator.isValid()) valid = false
        }
        return valid
    }
}