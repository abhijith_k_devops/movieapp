package com.cs.movieapp.utils


interface PreferenceHelper {


    fun setBooleanValue(key : String ,value: Boolean)

    fun getBooleanValue(key : String): Boolean

    fun setStringValue(key : String,value: String)

    fun getStringValue(key : String): String?

    fun setIntValue(key : String,value: Int)

    fun getIntValue(key : String): Int?

    fun clearPreference()

}