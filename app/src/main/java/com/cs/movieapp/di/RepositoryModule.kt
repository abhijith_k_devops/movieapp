package com.cs.movieapp.di


import com.cs.movieapp.data.repository.MovieRepository
import com.cs.movieapp.data.repository.MovieRepositoryImpl
import com.cs.movieapp.utils.PreferenceHelper
import com.cs.movieapp.utils.PreferenceHelperImpl
import org.koin.dsl.module

val repositoryModule = module {
    single { MovieRepositoryImpl(get(),get()) as MovieRepository }

    single { PreferenceHelperImpl(get()) as PreferenceHelper }
}
