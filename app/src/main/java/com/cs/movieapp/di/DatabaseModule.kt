package com.cs.movieapp.di

import com.cs.movieapp.data.local.db.MovieDb
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {

    single {
        MovieDb.getInstance(androidApplication())
    }
    single { get<MovieDb>().movieDao() }
}