package com.cs.movieapp.di


import com.cs.movieapp.ui.main.MainViewModel
import com.cs.movieapp.ui.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get(),get(),get()) }
    viewModel { SplashViewModel(get(),get()) }
}
