package com.cs.movieapp.di

val testMovieApp = listOf(
    databaseModule,
    repositoryModule,
    viewModelModule
)